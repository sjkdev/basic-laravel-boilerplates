<?php

namespace Imagine\Http\Controllers;

use Imagine\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        return view('home');
    }
}