Imagine
-------

Installation instructions

1. Create a fresh Laravel installation
2. Set the application namespace: php artisan app:name Imagine
3. Copy over the controller files to app/Http/Controllers
4. Copy over the views to resources/views
5. Download Foundation (http://foundation.zurb.com) and copy foundation.min.css to public/css
6. Copy the includes theme.css to public/css
5. Edit config/filesystems.php, adding your Amazon S3 bucket, region, and keys
