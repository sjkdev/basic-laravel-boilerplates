Chatty setup instructions
-------------------------

1. Build a fresh Laravel installation.
2. Run `php artisan app:name Chatty` from the root directory. This sets your application name.
3. Copy over `app`, `database` and `resources` folders, replacing those in the default Laravel installation.
4. Create a database for this project.
5. Add your database details in the `.env` file.
6. Run `php artisan migrate` from the project directory. This creates the database tables.